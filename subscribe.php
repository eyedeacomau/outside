<?php
error_reporting(E_ALL);

include($_SERVER['DOCUMENT_ROOT']."/json-rpc-php/JsonRpcClient.php");
include($_SERVER['DOCUMENT_ROOT']."/json-rpc-php/AuthenticatedJsonRpcClient.php");

$api_url = 'http://www.vision6.com.au/api/jsonrpcserver?version=3.0';
$api_key = 'ba3de98cd529cbc00fcee6322645a735ba3d83ccf3aa72e17c86b8072645f7d9';
$list_id = '566514';

$contact = array(
    'Email' => $_REQUEST['email'],
    'First Name' => $_REQUEST['first_name'],
    'Last Name' => $_REQUEST['last_name']
);

$client = new JsonRpcClient($api_url);
$client->subscribeContact($api_key,$list_id,$contact);

header("Location: /contact/subscribe/thanks");
?>

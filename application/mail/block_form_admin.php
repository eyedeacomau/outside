<?php 
defined('C5_EXECUTE') or die("Access Denied.");

$submittedData='';
foreach($questionAnswerPairs as $questionAnswerPair){
	$submittedData .= $questionAnswerPair['question']."\r\n".$questionAnswerPair['answerDisplay']."\r\n"."\r\n";
} 
$formDisplayUrl=URL::to('/dashboard/reports/forms') . '?qsid='.$questionSetId;

$body = t("
There has been a submission of the form %s through your concrete5 website.

%s

To view all of this form's submissions, visit %s 

", $formName, $submittedData, $formDisplayUrl);

$bodyHTML = '
	<html>
		<head>
			<style type="text/css">
				.ReadMsgBody { width: 100%; background-color: #f1f1f1;}
				.ExternalClass {width: 100%; background-color: #f1f1f1;}
				.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
				body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
				body {margin:0; padding:0;}
				table {border-spacing:0;}
				table td {border-collapse:collapse;}
				.yshortcuts a {border-bottom: none !important;}
				p,td,li {
					font-family: Arial, sans-serif;
					font-size: 12px;
				}
			</style>
		</head>
		<body style="width:100%; text-align:center; background-color:#ededed; font-family:Arial;">
			<table style="margin:50px auto;" cellpadding="0" cellspacing="0"><tr><td>
				<table style="border-bottom:2px solid #222222; background-color:#000000; color: #ffffff;" width="600" cellpadding="10" cellspacing="0" border="0">
					<tr>
						<td width="50%" style="width:50%;text-align:left;">
							<img src="http://'.$_SERVER['HTTP_HOST'].'/application/themes/outside/images/logo.png" width="200" style="width:200px;"/>
						</td>
						<td valign="middle">
							<h3 style="text-align:right; color: #ffffff">Thanks for your enquiry</h3>
						</td>
					</tr>
				</table>
				<table style="background-color:#ffffff;" width="600" cellpadding="10" cellspacing="0" border="0">
					<tr>
						<td style="text-align:left;">
							<p>An enquiry was made to Outside Products with the following information:</p>';
							foreach($questionAnswerPairs as $row) {
								if(strtolower($row['question']) == 'quote') {
									$quote = json_decode($row['answerDisplay']);
									if(count($quote) > 0) {
										$bodyHTML .= '
										<table cellpadding="5" cellspacing="0" border="0">
											<tr>
												<th width="70%" style="text-align: left; font-size: 16px;">Product</th>
												<th width="15%" style="text-align: left; font-size: 16px;">Size</th>
												<th width="15%" style="text-align: left; font-size: 16px;">Quantity</th>
											</tr>
										';
										foreach($quote as $row) {
											$bodyHTML .= '<tr>
												<td>'.$row->name.'</td>
												<td>'.$row->size.'</td>
												<td>'.$row->qty.'</td>
											</tr>';
										}
										$bodyHTML .= '</table><br /><br />';
									} else {
										$bodyHTML .= '<p>No products selected to include in quote</p><br />';
									}
									break;
								}
							}
							$bodyHTML .= '
							<table cellpadding="5" cellspacing="0" border="0">
								';
foreach($questionAnswerPairs as $row){
	if(strtolower($row['question']) != 'quote') $bodyHTML .= '
										<tr>
											<td width="140" valign="top">
												<strong>'.$row['question'].'</strong>
											</td>
											<td valign="top">
												'.nl2br($row['answerDisplay']).'
											</td>
										</tr>';
}
$bodyHTML .= '</table>
							<br><br>
							<p>To view all of this form\'s submissions, visit <a href="'.$formDisplayUrl.'">Your Form reports</a> section in your dashboard.</p>
						</td>
					</tr>
				</table>
			</td></tr></table>
		</body>
	</html>
';
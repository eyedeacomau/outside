<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
?>

<main>
	<?php
	$a = new Area('Main');
	$a->enableGridContainer();
	$a->display($c);
	?>
	<div id="results">
		<?php
		$a = new Area('Results');
		$a->enableGridContainer();
		$a->display($c);
		?>
		<div id="no-results" class="container" style="display: none;">
			<div class="row">
				<div class="col-xs-12">
					<p>No Products found</p>
				</div>
			</div>
		</div>
	</div>
</main>

<?php  $this->inc('elements/footer.php'); ?>

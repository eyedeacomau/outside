<?php
    defined('C5_EXECUTE') or die("Access Denied.");
    $u = new User();
    $classes = array();
    if($c->isEditMode()) $classes[] = 'edit';
    //if(substr_count($c->cPath,'/') == 1) $classes[] = 'home'; // multilingual
    if(substr_count($c->cPath,'/') == 1) $classes[] = 'top';
    if(is_null($c->cPath)) $classes[] = 'home';
    if($u->isLoggedIn()) $classes[] = 'loggedin';
?>
<!DOCTYPE html>
<html lang="<?=Localization::activeLanguage()?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if(false): ?>
        <link rel="stylesheet" type="text/css" href="<?=$view->getThemePath()?>/css/bootstrap-modified.css">
        <?php //echo $html->css($view->getStylesheet('main.less'))?>
    <?php endif; ?>
    <?php Loader::element('header_required', array('pageTitle' => isset($pageTitle) ? $pageTitle : '', 'pageDescription' => isset($pageDescription) ? $pageDescription : '', 'pageMetaKeywords' => isset($pageMetaKeywords) ? $pageMetaKeywords : ''));?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script>
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement('style')
            msViewportStyle.appendChild(
                document.createTextNode(
                    '@-ms-viewport{width:auto!important}'
                )
            )
            document.querySelector('head').appendChild(msViewportStyle)
        }
    </script>
    <link rel="stylesheet" type="text/css" href="<?=$view->getThemePath(); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$view->getThemePath(); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$view->getThemePath(); ?>/css/main.css">
    <link href="//fonts.googleapis.com/css?family=Cabin:400,400i,700,700i" rel="stylesheet">
    <?php if(!$u->isLoggedIn()): ?>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script type="text/javascript" src="<?=$view->getThemePath(); ?>/js/bootstrap.min.js"></script>
    <?php else: ?>

    <?php endif; ?>
    <script type="text/javascript" src="<?=$view->getThemePath(); ?>/js/js.cookie.js"></script>
    <script type="text/javascript" src="<?=$view->getThemePath(); ?>/js/grids.min.js"></script>
    <script type="text/javascript" src="<?=$view->getThemePath(); ?>/js/select2.min.js"></script>
    <script type="text/javascript" src="<?=$view->getThemePath(); ?>/js/main.js"></script>
    <script type="text/javascript">var switchTo5x=true;</script>
    <script type="text/javascript" id="st_insights_js" src="//w.sharethis.com/button/buttons.js?publisher=b4bb6df8-b606-44fb-8f12-d943dcaec8e6"></script>
    <script type="text/javascript">stLight.options({publisher: "b4bb6df8-b606-44fb-8f12-d943dcaec8e6", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>
</head>
<body class="<?=implode(' ', $classes);?>">
<div class="<?=$c->getPageWrapperClass()?>">
    <header id="hd">
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" id="menu-toggle" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="<?=$view->getThemePath(); ?>/images/logo.png" alt="Outside" /></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <?php
                        $a = new GlobalArea('Menu');
                        $a->display();
                    ?>
                </div>
            </div>
        </nav>
    </header>

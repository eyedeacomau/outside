<?php defined('C5_EXECUTE') or die("Access Denied.");

?>
    <footer id="ft">
        <div class="top">
            <div class="container">
            <?php
            $a = new GlobalArea('Footer Top');
            // $a->enableGridContainer(); // Grid containers don't work on global areas? not closing divs properly
            $a->display();
            ?>
            </div>
        </div>
        <div class="bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                    <?php
                    $a = new GlobalArea('Footer Left');
                    $a->display();
                    ?>
                    </div>
                    <div class="col-sm-4">
                    <?php
                    $a = new GlobalArea('Footer Middle');
                    $a->display();
                    ?>
                    </div>
                    <div class="col-sm-4 text-right">
                        <a id="back-to-top" href="#top">Back to top <i class="fa fa-arrow-up"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<?php Loader::element('footer_required'); ?>
</body>
</html>


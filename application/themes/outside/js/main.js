(function($) {
	$(document).ready(function() {
        var quote = Cookies.get('quote'),
            quote_form = $('#quote'),
            search = $('#search'),
            product = $('#product'),
            size = '';
            slidespeed = 400,
            page_size = 10,
            infinite_offset = 200;

        if(typeof quote == 'string') quote = $.parseJSON(quote);
        if(typeof quote != 'object') quote = [];


        function updateQuoteItems() {
            if(quote.length > 0) {
                var quote_menu_item = $('a.quote', '#navbar');
                if(quote_menu_item.length) {
                    if($('span.items', quote_menu_item).length) $('span.items', quote_menu_item).remove();
                    quote_menu_item.append($('<span />').addClass('items').text('(' + quote.length + ')'));
                }
            }
        }
        updateQuoteItems();
        if(quote_form.length) {
            var quote_list = $('tbody', '#quote-list');
            function updateQuote() {
                quote_form.find('textarea').val(JSON.stringify(quote));
                if(quote_list.length) {
                    quote_list.html('');
                    if(quote.length > 0) {
                        $.each(quote, function(i, item) {
                            var tr = $('<tr />').addClass('row' + i + ' product' + item.id).data('index', i);
                            tr.append($('<td />').addClass('product').text(item.name));
                            tr.append($('<td />').addClass('size').text(item.size));
                            tr.append($('<td />').addClass('quantity').append($('<input />').attr('type', 'text').addClass('form-control quantity').val(item.qty)));
                            tr.append($('<td />').addClass('delete').append($('<a />').attr('href','#delete'+ i).append($('<i />').addClass('fa fa-times'))));
                            quote_list.append(tr);
                        });
                    } else {
                        quote_list.append($('<tr />').append($('<td />').attr('colspan', '4').text('Please find a product to add to your quote')));
                    }
                }
                updateQuoteItems();
            }
            updateQuote();

            quote_list.on('input', 'input', function(e) {
                e.preventDefault();
                var row = $(this).closest('tr'),
                    index = row.data('index');
                if(typeof quote[index] == 'object') {
                    quote[index].qty = intval($(this).val());
                    Cookies.set('quote', quote, {expires: 1});
                    updateQuote();
                }
            }).on('click', 'input', function(e) {
                $(this).select();
            }).on('click', 'a', function(e) {
                e.preventDefault();
                var row = $(this).closest('tr'),
                    index = row.data('index');
                if(typeof quote[index] == 'object') {
                    quote.splice(index, 1);
                    Cookies.set('quote', quote, {expires: 1});
                    row.remove();
                    updateQuote();
                }
            });
        }
        if(product.length) {
            $('.sizes', product).on('click', 'a', function(e) {
                e.preventDefault();
                size = $(this).data('size');
                $('.size.row').removeClass('selected');
                $(this).closest('.size.row').addClass('selected');
                $('#selected-size').text(size);
                $('#select-quantity').slideDown();
            });
            $('.container.top .heading', product).on('click', 'a.btn', function(e) {
                e.preventDefault();
                var sizes = $('.sizes.row', product).first();
                if(sizes.length) {
                    $('html,body').animate({
                        scrollTop: intval(sizes.offset().top - 100)
                    }, 700, 'easeOutExpo');
                }
            });
            $('#select-quantity').on('click', 'a.btn', function(e) {
                e.preventDefault();
                var quantity = intval($('#quantity').val());
                quote.push({
                    id: product.data('id'),
                    name: $('#product-title').text(),
                    size: size,
                    qty: quantity
                });
                Cookies.set('quote', quote, {expires: 1});
                $('dd.size').text(size);
                $('dd.quantity').text(quantity);
                updateQuoteItems();
                $('#add-to-quote').modal();
            });
        }

        if(search.length) {
            var results = $('.product', '#results');
            var product_list = new Array();
            var exact_dropdown = $('#exact');
            if(results.length) {
                $.each(results, function(i, val) {
                    var link = $(val).find('a.thumb');
                    if(link.length == 1) {
                        product_list.push({
                            link: link.attr('href'),
                            name: $.trim(link.text())
                        });
                        exact_dropdown.append($('<option />').val(link.attr('href')).text(link.text()));
                    }
                });
            }
            if(!$('body.loggedin').length) { // Affix doesn't work with the version of bootstrap loaded by C5
                var carousel = 0;
                if($('.carousel').length) carousel = $('.carousel').outerHeight();
                search.affix({
                    offset: {
                        top: function() {
                            return (this.top = $('#hd').outerHeight() + carousel)
                        },
                        bottom: function() {
                            return (this.bottom = $('#ft').outerHeight())
                        }
                    }
                }).on('affix.bs.affix', function() {
                    if(window.innerWidth >= 768) $('main').css('paddingTop', search.outerHeight()+'px');
                }).on('affix-top.bs.affix', function() {
                    $('main').css('paddingTop', 0);
                });
            }
            function toggleGroup(group, show) {
                if(typeof show == 'undefined') show = true;
                if(typeof group == 'object') {
                    if(show) {
                        if(!group.hasClass('show')) {
                            group.slideDown(slidespeed, function() {
                                group.addClass('show');
                            });
                        }
                    } else {
                        if(group.hasClass('show')) {
                            group.slideUp(slidespeed, function() {
                                group.removeClass('show');
                                var select = group.find('select');
                                if(select.length) select.val(null).trigger('change'); // Possible race condition here if not for hasClass('show') because select change triggers updateSearchState which triggers toggleGroup which triggers this
                            });
                        }
                    }
                }
            }
            function updateSearchState() {
                var category = $('#category'),
                    exact_group = $('#exact-group'),
                    material_group = $('#material-group'),
                    roof_group = $('#roof-group');
                if(category.val() == 'exact') {
                    toggleGroup(exact_group, true);
                    toggleGroup(material_group, false);
                } else {
                    toggleGroup(exact_group, false);
                    toggleGroup(material_group, true);
                }
                var selected = $.trim(category.find('option:selected').text().toLowerCase());
                if(selected == 'shelters' || selected == 'restrooms') toggleGroup(roof_group, true);
                else toggleGroup(roof_group, false);
            }
            updateSearchState();
            function showProducts(products) {
                if(typeof products == 'object' && products.length > 0) {
                    $.each(products.filter(':not(.loaded)'), function(i, val) {
                        var product = $(val),
                            thumb = product.find('a.thumb');
                        if(thumb.length) {
                            thumb.css('backgroundImage', 'url('+thumb.data('background')+')');
                            product.addClass('loaded');
                        }
                    });
                    products.show();
                }
            }
            function doSearch() {
                var category = $('#category').val(),
                    material = $('#material').val(),
                    roof = $('#roof').val(),
                    exact = $('#exact').val(),
                    show_all = false;
                if(category == 'exact') {
                    if(typeof exact != 'undefined' && exact != '') window.location.href = exact;
                    return false;
                }

                if($('body').hasClass('top')) show_all = true; // top == top level page, ie not homepage, so products page
                show_all = true; // Allow people to search without criteria
                if(category != '' || material != '' || roof != '' || show_all) {
                    $('#results').show();
                    results.removeClass('found').hide();
                    var found = $([]);
                    $.each(results, function(i, val) {
                        var product = $(val);
                        if((category == '' || product.hasClass(category)) && (material == '' || product.hasClass(material)) && (roof == '' || product.hasClass(roof))) found.push(product[0]);
                    });
                    if(found.length) {
                        found.addClass('found');
                        showProducts(found.slice(0, page_size));
                        $('#no-results').slideUp();
                    } else {
                        $('#no-results').slideDown();
                    }
                }
            }
            if($('body').hasClass('top')) doSearch(); // top == top level page, ie not homepage, so products page
            search.on('click', '.btn', function(e) {
                e.preventDefault();
                doSearch();
            }).on('change', 'select', function(e) {
                if($(this).attr('id') == 'exact' && $(this).val() != '') {
                    window.location.href = $(this).val();
                    return false;
                }
                updateSearchState();
                doSearch();
            });
            doSearch();
            $(window).scroll(function() {
                var results_area = $('#results'),
                    scrollHeight = (intval($(this).scrollTop()) + intval($(this).outerHeight())),
                    resultsAt = (intval(results_area.offset().top) + results_area.outerHeight());
                if((resultsAt - infinite_offset) < scrollHeight) showProducts(results.filter('.found:not(:visible)').slice(0, page_size));
            });
        }
        $('select.select2').select2();
        $('#ft').on('click', '#back-to-top', function(e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700, 'easeOutExpo');
        });

		function updatePositions() {

		}
		updatePositions();
		var resize_timer;
		$(window).resize(function() {
			clearTimeout(resize_timer);
			resize_timer = setTimeout(updatePositions, 100);
		});
	});
})(jQuery);

function getHashFilter() {
  var matches = location.hash.match(/filter=([^&]+)/i); // get filter=filterName
  var hashFilter = matches && matches[1];
  return hashFilter && decodeURIComponent(hashFilter);
}
function intval(v) {
    v = parseInt(v);
    return isNaN(v) ? 0 : v;
}
function floatval(v) {
    v = parseFloat(v);
    return isNaN(v) ? 0 : v;
}
function getDigits(v) {
    return v.replace(/[^\d]/g, '');
}
function slug(string) {
    if(typeof string == 'undefined') string = '';
    return string.replace(/(^\-+|[^a-zA-Z0-9\/_| -]+|\-+$)/g, '').toLowerCase().replace(/[\/_| -]+/g, '-');
}
function randomint(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
Number.prototype.map = function(in_min, in_max, out_min, out_max) {
    return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
Number.prototype.formatMoney = function(c, d, t) { /* (decimal places, decimal, thousands separator */
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
function getMoney(strMoney) {
    return floatval(floatval(jQuery.trim(strMoney.replace(/[^\d\.]/g, ''))).toFixed(2));
}
function shuffle(v) {
    for(var j, x, i = v.length; i; j = parseInt(Math.random() * i), x = v[--i], v[i] = v[j], v[j] = x);
    return v;
}
function capitalize(str) {
    return str.replace(/\w\S*/g, function(txt) {return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
jQuery.easing.easeOutExpo = function (x, t, b, c, d) {
	return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
}


<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
$c = Page::getCurrentPage();
$title = 'Product';
$category = 'category';
$material = 'material';
$roof = 'roof';
$pdf = false;
if($c instanceof Page) {
	$title = $c->getCollectionName();
	$category = $c->getAttribute('category', 'display');
	$material = $c->getAttribute('material', 'display');
	$roof = $c->getAttribute('roof', 'display');
	$pdf = $c->getAttribute('pdf');
}
if($pdf != false) $pdf = $pdf->getURL();
$product_image = $c->getAttribute('product_image');
if($product_image) $product_image = $product_image->getURL();

$id = $c->cID;
?>

<main id="product" data-id="<?=$id;?>">
	<?php
	$a = new Area('Banner');
	$a->enableGridContainer();
	$a->display($c);
	?>
	<div class="container top">
		<div class="row heading">
			<div class="col-sm-7 col-md-7 col-lg-8">
				<h1 id="product-title"><?=$title;?></h1>
			</div>
			<div class="col-sm-5 col-md-5 col-lg-4">
				<a href="/quote" class="btn btn-primary">Request a Quote</a>
			</div>
		</div>
		<div class="row lines">
			<div class="col-xs-6 col-sm-7 col-md-7 col-lg-8">
				<ul class="clearfix">
					<li><strong>Category:</strong> <?=$category;?></li>
					<li><strong>Material:</strong> <?=$material;?></li>
					<?php if(!empty($roof)): ?>
						<li><strong>Roof:</strong> <?=$roof;?></li>
					<?php endif; ?>
				</ul>
			</div>
			<div class="col-xs-6 col-sm-5 col-md-5 col-lg-4">
				<div class="actions row">
					<div class="col-sm-6">
						<a href="/"><i class="fa fa-search"></i> Back to search</a>
					</div>
					<?php if($pdf != false): ?>
					<div class="col-sm-4">
					<?php else: ?>
					<div class="col-sm-6 text-right">
					<?php endif; ?>
						<span class="st_sharethis" displayText="Share" st_title="<?=$title;?>"<?=($product_image != false ? ' st_image="'.$product_image.'"' : '');?>></span>
					</div>
					<?php if($pdf != false): ?>
					<div class="col-sm-2 text-right">
						<a href="<?=$pdf;?>" class="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="container hero">
		<div class="row">
			<div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2">
				<?php
				$a = new Area('Hero');
				$a->display($c);
				?>
			</div>
		</div>
	</div>
	<?php
	$a = new Area('Main');
	$a->enableGridContainer();
	$a->display($c);
	?>
	<div class="container meta">
		<div class="row">
			<div class="col-sm-6 left">
				<?php
				$a = new Area('Left');
				$a->display($c);
				?>
			</div>
			<div class="col-sm-6 right">
				<?php
				$a = new Area('Right');
				$a->display($c);
				?>
			</div>
		</div>
	</div>
	<?php
	$a = new GlobalArea('Product Files');
	$a->enableGridContainer();
	$a->display($c);
	?>
	<?php
	$a = new Area('Page Bottom');
	$a->enableGridContainer();
	$a->display($c);
	?>
</main>
<div class="modal fade" id="add-to-quote" tabindex="-1" role="dialog" aria-labelledby="add-to-quote-title">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="add-to-quote-title">Product added to quote</h4>
			</div>
			<div class="modal-body">
				<h2><?=$title;?></h2>
				<?php if($product_image != false): ?>
					<img src="<?=$product_image;?>" />
				<?php endif; ?>
				<dl>
					<dt class="size">Size:</dt>
					<dd class="size"></dd>
					<dt class="quantity">Quantity:</dt>
					<dd class="quantity"></dd>
				</dl>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Continue looking around</button>
				<a href="/quote" class="btn btn-primary">Get Quote</a>
			</div>
		</div>
	</div>
</div>
<?php  $this->inc('elements/footer.php'); ?>

<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php  $fileone_o = null;
if ($fileone > 0) {
    $fileone_o = File::getByID($fileone);
} ?>
<div class="form-group">
    <?php  echo $form->label('fileone', t("File 1")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('fileone', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/concrete/asset_library")->file($view->field('ccm-b-file-fileone'), "fileone", t("Choose File"), $fileone_o); ?>
</div>
<div class="form-group">
    <?php  echo $form->label('fileone_title', t("File 1") . " " . t("Title")); ?>
    <?php  echo $form->text($view->field('fileone_title'), $fileone_title, array (
  'maxlength' => 255,
  'placeholder' => NULL,
)); ?>
</div>

<?php  $filetwo_o = null;
if ($filetwo > 0) {
    $filetwo_o = File::getByID($filetwo);
} ?>
<div class="form-group">
    <?php  echo $form->label('filetwo', t("File 2")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('filetwo', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/concrete/asset_library")->file($view->field('ccm-b-file-filetwo'), "filetwo", t("Choose File"), $filetwo_o); ?>
</div>
<div class="form-group">
    <?php  echo $form->label('filetwo_title', t("File 2") . " " . t("Title")); ?>
    <?php  echo $form->text($view->field('filetwo_title'), $filetwo_title, array (
  'maxlength' => 255,
  'placeholder' => NULL,
)); ?>
</div>

<?php  $filethree_o = null;
if ($filethree > 0) {
    $filethree_o = File::getByID($filethree);
} ?>
<div class="form-group">
    <?php  echo $form->label('filethree', t("File 3")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('filethree', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/concrete/asset_library")->file($view->field('ccm-b-file-filethree'), "filethree", t("Choose File"), $filethree_o); ?>
</div>
<div class="form-group">
    <?php  echo $form->label('filethree_title', t("File 3") . " " . t("Title")); ?>
    <?php  echo $form->text($view->field('filethree_title'), $filethree_title, array (
  'maxlength' => 255,
  'placeholder' => NULL,
)); ?>
</div>
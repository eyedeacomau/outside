<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="files">
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<?php if(isset($fileone) && $fileone !== false): ?>
					<a href="<?php echo isset($fileone->urls["download"]) ? $fileone->urls["download"] : $fileone->urls["relative"]; ?>" target="_blank" class="btn btn-default">
						<?=isset($fileone_title) && trim($fileone_title) != "" ? h($fileone_title) : $fileone->getTitle(); ?> <i class="fa fa-file-pdf-o"></i>
					</a>
				<?php endif; ?>
			</div>
			<div class="col-sm-4">
				<?php if(isset($filetwo) && $filetwo !== false): ?>
					<a href="<?php echo isset($filetwo->urls["download"]) ? $filetwo->urls["download"] : $filetwo->urls["relative"]; ?>" target="_blank" class="btn btn-default">
						<?=isset($filetwo_title) && trim($filetwo_title) != "" ? h($filetwo_title) : $filetwo->getTitle(); ?> <i class="fa fa-file-pdf-o"></i>
					</a>
				<?php endif; ?>
			</div>
			<div class="col-sm-4">
				<?php if(isset($filethree) && $filethree !== false): ?>
					<a href="<?php echo isset($filethree->urls["download"]) ? $filethree->urls["download"] : $filethree->urls["relative"]; ?>" target="_blank" class="btn btn-default">
						<?=isset($filethree_title) && trim($filethree_title) != "" ? h($filethree_title) : $filethree->getTitle(); ?> <i class="fa fa-file-pdf-o"></i>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php  namespace Application\Block\AdditionalFiles;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use File;
use Page;
use Permissions;
use URL;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array();
    protected $btExportFileColumns = array('fileone', 'filetwo', 'filethree');
    protected $btTable = 'btAdditionalFiles';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = true;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Additional Files");
    }

    public function view()
    {
        $fileone_id = (int)$this->fileone;
        $this->fileone = false;
        if ($fileone_id > 0 && ($fileone_file = File::getByID($fileone_id)) && is_object($fileone_file)) {
            $fp = new Permissions($fileone_file);
	        if ($fp->canViewFile()) {
	            $urls = array('relative' => $fileone_file->getRelativePath());
		        $c = Page::getCurrentPage();
		        if ($c instanceof Page) {
			        $cID = $c->getCollectionID();
			        $urls['download'] = URL::to('/download_file', $fileone_id, $cID);
		        }
		        $fileone_file->urls = $urls;
		        $this->fileone = $fileone_file;
            }
        }
        $this->set("fileone", $this->fileone);
        $filetwo_id = (int)$this->filetwo;
        $this->filetwo = false;
        if ($filetwo_id > 0 && ($filetwo_file = File::getByID($filetwo_id)) && is_object($filetwo_file)) {
            $fp = new Permissions($filetwo_file);
	        if ($fp->canViewFile()) {
	            $urls = array('relative' => $filetwo_file->getRelativePath());
		        $c = Page::getCurrentPage();
		        if ($c instanceof Page) {
			        $cID = $c->getCollectionID();
			        $urls['download'] = URL::to('/download_file', $filetwo_id, $cID);
		        }
		        $filetwo_file->urls = $urls;
		        $this->filetwo = $filetwo_file;
            }
        }
        $this->set("filetwo", $this->filetwo);
        $filethree_id = (int)$this->filethree;
        $this->filethree = false;
        if ($filethree_id > 0 && ($filethree_file = File::getByID($filethree_id)) && is_object($filethree_file)) {
            $fp = new Permissions($filethree_file);
	        if ($fp->canViewFile()) {
	            $urls = array('relative' => $filethree_file->getRelativePath());
		        $c = Page::getCurrentPage();
		        if ($c instanceof Page) {
			        $cID = $c->getCollectionID();
			        $urls['download'] = URL::to('/download_file', $filethree_id, $cID);
		        }
		        $filethree_file->urls = $urls;
		        $this->filethree = $filethree_file;
            }
        }
        $this->set("filethree", $this->filethree);
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
    }

    protected function addEdit()
    {
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("fileone", $this->btFieldsRequired) && (!isset($args["fileone"]) || trim($args["fileone"]) == "" || !is_object(File::getByID($args["fileone"])))) {
            $e->add(t("The %s field is required.", t("File 1")));
        }
        if (in_array("filetwo", $this->btFieldsRequired) && (!isset($args["filetwo"]) || trim($args["filetwo"]) == "" || !is_object(File::getByID($args["filetwo"])))) {
            $e->add(t("The %s field is required.", t("File 2")));
        }
        if (in_array("filethree", $this->btFieldsRequired) && (!isset($args["filethree"]) || trim($args["filethree"]) == "" || !is_object(File::getByID($args["filethree"])))) {
            $e->add(t("The %s field is required.", t("File 3")));
        }
        return $e;
    }

    public function composer()
    {
        $this->edit();
    }
}
<?php defined('C5_EXECUTE') or die("Access Denied.");
$navigationTypeText = ($navigationType == 0) ? 'arrows' : 'pages';
$c = Page::getCurrentPage();
$blockID = 'slider'.$this->block->getBlockID();

if($c->isEditMode()): ?>
    <div class="ccm-edit-mode-disabled-item" style="<?php echo isset($width) ? "width: $width;" : '' ?><?php echo isset($height) ? "height: $height;" : '' ?>">
        <div style="padding: 40px 0px;"><?php echo t('Image Slider disabled in edit mode.')?></div>
    </div>
<?php elseif(count($rows) == 0): ?>
    <div class="ccm-edit-mode-disabled-item" style="<?php echo isset($width) ? "width: $width;" : '' ?><?php echo isset($height) ? "height: $height;" : '' ?>">
        <div style="padding: 40px 0px;"><?php echo t('No slides added yet.')?></div>
    </div>
<?php else: ?>
<div id="<?=$blockID;?>" class="carousel slide <?=($navigationType == 0 ? 'nav' : 'pager');?>" data-ride="carousel" data-interval="<?=$timeout;?>" data-pause="<?=($pause ? 'hover' : 'null');?>">
    <ol class="carousel-indicators">
        <?php $i = 0; foreach($rows as $row): ?>
        <li data-target="<?=$blockID;?>" data-slide-to="<?=$i;?>"<?=($i == 0 ? ' class="active"' : '');?>></li>
        <?php $i++; endforeach; ?>
    </ol>
    <div class="carousel-inner" role="listbox">
        <?php $i = 0; foreach($rows as $row): ?>
        <?php
            $f = File::getByID($row['fID']);
            $image = $f->getURL();
        ?>
        <<?=($row['linkURL'] ? 'a href="'.$row['linkURL'].'"' : 'div');?> class="item<?=($i == 0 ? ' active' : '');?>" style="background-image: url(<?=$image;?>);">
            <img src="<?=$image;?>" class="img-responsive" />
            <?php if((isset($row['title']) && !empty($row['title'])) || (isset($row['description']) && !empty($row['description']))): ?>
                <div class="carousel-caption">
                    <?php if(isset($row['title']) && !empty($row['title'])): ?>
                        <h2><?=$row['title'];?></h2>
                    <?php endif; ?>
                    <?php if(isset($row['description']) && !empty($row['description'])): ?>
                        <?=$row['description'];?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </<?=($row['linkURL'] ? 'a' : 'div');?>>
        <?php $i++; endforeach; ?>
    </div>
    <a class="left carousel-control" href="#<?=$blockID;?>" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#<?=$blockID;?>" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<?php endif; ?>

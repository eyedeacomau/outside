(function($) {
	$(document).ready(function() {
        var header_overlap = false;
        function updateSlider(slider) {
            if(typeof slider === 'undefined') slider = $('.carousel').first();
            if(slider.length) {
                var hd = $('nav.navbar-default', '#hd').outerHeight();
                if(slider.prev().length || !slider.parent().is('main')) hd = 0;
                if(!header_overlap) hd = 0;

                var carousel_height = slider.outerHeight() - hd;
                var carousel_content = $('.carousel-caption', slider);
                if(carousel_content.length) carousel_content.each(function(i, val) {
                    $(val).css('top', intval((carousel_height - $(val).outerHeight()) /2 + hd)+'px');
                });
            }
        }
		function updatePositions() {
            var sliders = $('.carousel');
            if(sliders.length) $.each(sliders, function(i, val) {
                updateSlider($(val));
            });
		}
		updatePositions();
		var resize_timer;
		$(window).resize(function() {
			clearTimeout(resize_timer);
			resize_timer = setTimeout(updatePositions, 100);
		});
	});
})(jQuery);
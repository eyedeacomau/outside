<?php  namespace Application\Block\Sizes;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Database;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('sizes' => array('size'));
    protected $btExportFileColumns = array();
    protected $btExportTables = array('btSizes', 'btSizesSizesEntries');
    protected $btTable = 'btSizes';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Sizes");
    }

    public function getSearchableContent()
    {
        $content = array();
        $db = Database::connection();
        $sizes_items = $db->fetchAll('SELECT * FROM btSizesSizesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($sizes_items as $sizes_item_k => $sizes_item_v) {
            if (isset($sizes_item_v["size"]) && trim($sizes_item_v["size"]) != "") {
                $content[] = $sizes_item_v["size"];
            }
            if (isset($sizes_item_v["extra"]) && trim($sizes_item_v["extra"]) != "") {
                $content[] = $sizes_item_v["extra"];
            }
        }
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $sizes = array();
        $sizes_items = $db->fetchAll('SELECT * FROM btSizesSizesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($sizes_items as $sizes_item_k => &$sizes_item_v) {
            if (!isset($sizes_item_v["extracheck"]) || trim($sizes_item_v["extracheck"]) == "") {
                $sizes_item_v["extracheck"] = '0';
            }
        }
        $this->set('sizes_items', $sizes_items);
        $this->set('sizes', $sizes);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btSizesSizesEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $sizes_items = $db->fetchAll('SELECT * FROM btSizesSizesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($sizes_items as $sizes_item) {
            unset($sizes_item['id']);
            $sizes_item['bID'] = $newBID;
            $db->insert('btSizesSizesEntries', $sizes_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $sizes = $this->get('sizes');
        $this->set('sizes_items', array());
        
        $this->set('sizes', $sizes);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $sizes = $this->get('sizes');
        $sizes_items = $db->fetchAll('SELECT * FROM btSizesSizesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $this->set('sizes', $sizes);
        $this->set('sizes_items', $sizes_items);
    }

    protected function addEdit()
    {
        $sizes = array();
        $this->set('sizes', $sizes);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = \Concrete\Core\Asset\AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/sizes/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/sizes/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/sizes/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btSizesSizesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $sizes_items = isset($args['sizes']) && is_array($args['sizes']) ? $args['sizes'] : array();
        $queries = array();
        if (!empty($sizes_items)) {
            $i = 0;
            foreach ($sizes_items as $sizes_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($sizes_item['size']) && trim($sizes_item['size']) != '') {
                    $data['size'] = trim($sizes_item['size']);
                } else {
                    $data['size'] = null;
                }
                if (isset($sizes_item['extra']) && trim($sizes_item['extra']) != '') {
                    $data['extra'] = trim($sizes_item['extra']);
                } else {
                    $data['extra'] = null;
                }
                if (isset($sizes_item['extracheck']) && trim($sizes_item['extracheck']) != '' && in_array($sizes_item['extracheck'], array(0, 1))) {
                    $data['extracheck'] = $sizes_item['extracheck'];
                } else {
                    $data['extracheck'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btSizesSizesEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btSizesSizesEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btSizesSizesEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        $sizesEntriesMin = 0;
        $sizesEntriesMax = 0;
        $sizesEntriesErrors = 0;
        $sizes = array();
        if (isset($args['sizes']) && is_array($args['sizes']) && !empty($args['sizes'])) {
            if ($sizesEntriesMin >= 1 && count($args['sizes']) < $sizesEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("Sizes"), $sizesEntriesMin, count($args['sizes'])));
                $sizesEntriesErrors++;
            }
            if ($sizesEntriesMax >= 1 && count($args['sizes']) > $sizesEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("Sizes"), $sizesEntriesMax, count($args['sizes'])));
                $sizesEntriesErrors++;
            }
            if ($sizesEntriesErrors == 0) {
                foreach ($args['sizes'] as $sizes_k => $sizes_v) {
                    if (is_array($sizes_v)) {
                        if (in_array("size", $this->btFieldsRequired['sizes']) && (!isset($sizes_v['size']) || trim($sizes_v['size']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Size"), t("Sizes"), $sizes_k));
                        }
                        if (in_array("extra", $this->btFieldsRequired['sizes']) && (!isset($sizes_v['extra']) || trim($sizes_v['extra']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Extra"), t("Sizes"), $sizes_k));
                        }
                        if (in_array("extracheck", $this->btFieldsRequired['sizes']) && (!isset($sizes_v['extracheck']) || trim($sizes_v['extracheck']) == "" || !in_array($sizes_v['extracheck'], array(0, 1)))) {
                            $e->add(t("The %s field is required.", t("Extracheck"), t("Sizes"), $sizes_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('Sizes'), $sizes_k));
                    }
                }
            }
        } else {
            if ($sizesEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("Sizes"), $sizesEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = \Concrete\Core\Asset\AssetList::getInstance();
        $al->register('javascript', 'auto-js-sizes', 'blocks/sizes/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-sizes');
        $this->edit();
    }
}
<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if(!empty($sizes_items)): ?>
	<p><strong>Please select a size to request a quote</strong></p>
	<?php foreach(array_chunk($sizes_items, 2) as $row): ?>
		<div class="sizes row">
			<?php foreach($row as $key => $sizes_item): ?>
				<?php if(isset($sizes_item["size"]) && trim($sizes_item["size"]) != ""): ?>
					<div class="col-xs-6">
						<div class="size row">
							<div class="col-sm-12">
								<a href="#size" class="size" data-size="<?=h($sizes_item['size']);?>"><i class="fa fa-chevron-right green"></i> <?=h($sizes_item["size"]); ?></a>
							</div>
							<div class="col-sm-6" style="display: none;">
								<a href="#size" class="btn btn-primary" data-size="<?=h($sizes_item['size']);?>">Add to quote</a>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if(false): ?>
					<?php if(isset($sizes_item["extra"]) && trim($sizes_item["extra"]) != ""): ?>
						<?=h($sizes_item["extra"]); ?>
					<?php endif; ?>
					<?php if(isset($sizes_item["extracheck"]) && trim($sizes_item["extracheck"]) != ""): ?>
						<?=$sizes_item["extracheck"] == 1 ? t("Yes") : t("No"); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
	<?php endforeach; ?>
	<div id="select-quantity" style="display: none;">
		<div class="selected-size row">
			<div class="col-xs-6 col-sm-3 col-md-3">
				<label class="control-label" for="size">Size</label>
			</div>
			<div id="selected-size" class="col-xs-6 col-sm-9 col-md-9">

			</div>
		</div>
		<div class="quantity row">
			<div class="col-xs-12 col-sm-3 col-md-3">
				<label class="control-label" for="quantity">Quantity</label>
			</div>
			<div class="col-xs-6 col-sm-4 col-md-5">
				<input type="number" name="quantity" step="1" id="quantity" class="form-control" value="1" />
			</div>
			<div class="col-xs-6 col-sm-5 col-md-4">
				<a href="#add-to-quote" class="btn btn-primary">Add to quote</a>
			</div>
		</div>
	</div>
<?php endif; ?>
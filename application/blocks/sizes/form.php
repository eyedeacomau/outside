<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php  echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php  $repeatable_container_id = 'btSizes-sizes-container-' . Core::make('helper/validation/identifier')->getString(18); ?>
    <div id="<?php  echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php  echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php  echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $sizes_items,
                        'order' => array_keys($sizes_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php  echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php  echo t('Sizes') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php  echo $view->field('sizes'); ?>[{{id}}][size]" class="control-label"><?php  echo t("Size"); ?></label>
    <?php  echo isset($btFieldsRequired['sizes']) && in_array('size', $btFieldsRequired['sizes']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php  echo $view->field('sizes'); ?>[{{id}}][size]" id="<?php  echo $view->field('sizes'); ?>[{{id}}][size]" class="form-control title-me" type="text" value="{{ size }}" maxlength="255" />
</div>            <div class="form-group" style="display: none;">
    <label for="<?php  echo $view->field('sizes'); ?>[{{id}}][extra]" class="control-label"><?php  echo t("Extra"); ?></label>
    <?php  echo isset($btFieldsRequired['sizes']) && in_array('extra', $btFieldsRequired['sizes']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php  echo $view->field('sizes'); ?>[{{id}}][extra]" id="<?php  echo $view->field('sizes'); ?>[{{id}}][extra]" class="form-control" type="text" value="{{ extra }}" maxlength="255" />
</div>            <div class="form-group" style="display: none;">
    <label for="<?php  echo $view->field('sizes'); ?>[{{id}}][extracheck]" class="control-label"><?php  echo t("Extracheck"); ?></label>
    <?php  echo isset($btFieldsRequired) && in_array('extracheck', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  $sizesExtracheck_options = (isset($btFieldsRequired['sizes']) && in_array('extracheck', $btFieldsRequired['sizes']) ? array() : array("" => "--" . t("Select") . "--")) + array(0 => t("No"), 1 => t("Yes")); ?>
                    <select name="<?php  echo $view->field('sizes'); ?>[{{id}}][extracheck]" id="<?php  echo $view->field('sizes'); ?>[{{id}}][extracheck]" class="form-control">{{#select extracheck}}<?php  foreach ($sizesExtracheck_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php  echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btSizes.sizes.edit.open', {id: '<?php  echo $repeatable_container_id; ?>'});
    $.each($('#<?php  echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>
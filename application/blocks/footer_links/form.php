<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<div class="form-group">
    <?php  echo $form->label('linkone', t("Link 1")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('linkone', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/form/page_selector")->selectPage($view->field('linkone'), $linkone); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('linkone_text', t("Link 1") . " " . t("Text")); ?>
    <?php  echo $form->text($view->field('linkone_text'), $linkone_text, array()); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('linktwo', t("Link 2")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('linktwo', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/form/page_selector")->selectPage($view->field('linktwo'), $linktwo); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('linktwo_text', t("Link 2") . " " . t("Text")); ?>
    <?php  echo $form->text($view->field('linktwo_text'), $linktwo_text, array()); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('linkthree', t("Link 3")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('linkthree', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/form/page_selector")->selectPage($view->field('linkthree'), $linkthree); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('linkthree_text', t("Link 3") . " " . t("Text")); ?>
    <?php  echo $form->text($view->field('linkthree_text'), $linkthree_text, array()); ?>
</div>
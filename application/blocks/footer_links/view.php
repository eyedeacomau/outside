<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="row">
	<div class="col-sm-4">
		<?php if(!empty($linkone) && ($linkone_c = Page::getByID($linkone)) && !$linkone_c->error && !$linkone_c->isInTrash()): ?>
			<?='<a href="' . $linkone_c->getCollectionLink() . '" class="btn btn-default">' . (isset($linkone_text) && trim($linkone_text) != "" ? $linkone_text : $linkone_c->getCollectionName()) . '</a>'; ?>
		<?php endif; ?>
	</div>
	<div class="col-sm-4">
		<?php if(!empty($linktwo) && ($linktwo_c = Page::getByID($linktwo)) && !$linktwo_c->error && !$linktwo_c->isInTrash()): ?>
			<?='<a href="' . $linktwo_c->getCollectionLink() . '" class="btn btn-default">' . (isset($linktwo_text) && trim($linktwo_text) != "" ? $linktwo_text : $linktwo_c->getCollectionName()) . '</a>'; ?>
		<?php endif; ?>
	</div>
	<div class="col-sm-4">
		<?php if(!empty($linkthree) && ($linkthree_c = Page::getByID($linkthree)) && !$linkthree_c->error && !$linkthree_c->isInTrash()): ?>
			<?='<a href="' . $linkthree_c->getCollectionLink() . '" class="btn btn-default">' . (isset($linkthree_text) && trim($linkthree_text) != "" ? $linkthree_text : $linkthree_c->getCollectionName()) . '</a>'; ?>
		<?php endif; ?>
	</div>
</div>
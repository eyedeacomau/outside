<?php  namespace Application\Block\FooterLinks;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Page;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array();
    protected $btExportFileColumns = array();
    protected $btTable = 'btFooterLinks';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Footer Links");
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
    }

    protected function addEdit()
    {
        $this->set('btFieldsRequired', $this->btFieldsRequired);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("linkone", $this->btFieldsRequired) && (trim($args["linkone"]) == "" || $args["linkone"] == "0" || (($page = Page::getByID($args["linkone"])) && $page->error !== false))) {
            $e->add(t("The %s field is required.", t("Link 1")));
        }
        if (in_array("linktwo", $this->btFieldsRequired) && (trim($args["linktwo"]) == "" || $args["linktwo"] == "0" || (($page = Page::getByID($args["linktwo"])) && $page->error !== false))) {
            $e->add(t("The %s field is required.", t("Link 2")));
        }
        if (in_array("linkthree", $this->btFieldsRequired) && (trim($args["linkthree"]) == "" || $args["linkthree"] == "0" || (($page = Page::getByID($args["linkthree"])) && $page->error !== false))) {
            $e->add(t("The %s field is required.", t("Link 3")));
        }
        return $e;
    }

    public function composer()
    {
        $this->edit();
    }
}
<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$c = Page::getCurrentPage();
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */
?>
<?php if($c->isEditMode() && !$controller->isBlockEmpty()): ?>
	<div class="ccm-edit-mode-disabled-item"><?php echo t('Page List Edit Mode.')?></div>
<?php elseif($c->isEditMode() && $controller->isBlockEmpty()): ?>
	<div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php elseif(count($pages) == 0): ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<p>No results</p>
			</div>
		</div>
	</div>
<?php else: ?>
	<div class="container-fluid">
		<div class="row">
			<?php foreach($pages as $page): ?>
				<?php
					$title = $page->getCollectionName();
					$url = ($page->getCollectionPointerExternalLink() != '') ? $page->getCollectionPointerExternalLink() : $nh->getLinkToCollection($page);
					$custom = (intval($page->getAttribute('custom')) == 1 ? true : false); // Checkboxes can return 1 (ticked) 0 (unticked) false (not set)

					$categories = array();
					$category_topics = $page->getAttribute('category', 'value');
					if(!empty($category_topics)) foreach($category_topics as $topic) $categories[] = array(
						'id' => 'category'.$topic->treeNodeID,
						'name' => $topic->treeNodeTopicName
					);

					$materials = array();
					$material_topics = $page->getAttribute('material', 'value');
					if(!empty($material_topics)) foreach($material_topics as $topic) $materials[] = array(
						'id' => 'material'.$topic->treeNodeID,
						'name' => $topic->treeNodeTopicName
					);

					$roofs = array();
					$roof_topics = $page->getAttribute('roof', 'value');
					if(!empty($roof_topics)) foreach($roof_topics as $topic) $roofs[] = array(
						'id' => 'roof'.$topic->treeNodeID,
						'name' => $topic->treeNodeTopicName
					);

					$pdf = $page->getAttribute('pdf');

					$classes = array();
					foreach(array_merge($categories, $materials, $roofs) as $class) $classes[] = $class['id'];
					if($custom) $classes[] = 'custom';
					if($pdf) $classes[] = 'pdf';


					$product_image = $page->getAttribute('product_image');
					if($product_image) $product_image = $product_image->getURL();
				?>
				<div class="col-xs-12 col-sm-4 col-md-3 product <?=implode(' ', $classes);?>" style="display: none;">
					<a href="<?=$url;?>" class="thumb" data-background="<?=$product_image; ?>" style="background-image: url(<?=$view->getThemePath();?>/images/placeholder.png);">
						<h2><?=$title;?></h2>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>

<?php  namespace Application\Block\ProductSearch;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Concrete\Core\Tree\Tree;
use Concrete\Core\Tree\Type\Topic as TopicTree;
use Concrete\Core\Tree\Type\Topic;
use Core;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('category', 'material', 'roof', 'exactmatch', 'searchbutton');
    protected $btExportFileColumns = array();
    protected $btTable = 'btProductSearch';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;

    public function getTopics($topic_name){
        $this->requireAsset('core/topics');
        $tt = new TopicTree();
        $tree = $tt->getByName($topic_name);
        if(stripos($topic_name, 'roof') !== false) $topic_name = 'roof';
        $node = $tree->getRootTreeNodeObject();
        $node->populateChildren();
        $topics = array();

        foreach($node->getChildNodes() as $topic) {
            if($topic instanceof \Concrete\Core\Tree\Node\Type\Topic) {
                $topics[] = array(
                    'id'   => $topic_name.$topic->getTreeNodeID(),
                    'name' => $topic->getTreeNodeDisplayName(),
                );
            }
        }

        return $topics;
    }
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Product Search");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->category;
        $content[] = $this->material;
        $content[] = $this->roof;
        $content[] = $this->exactmatch;
        $content[] = $this->searchbutton;
        return implode(" ", $content);
    }

    public function view()
    {
        if (trim($this->category) == "") {
            $this->set("category", 'I&#039;m looking for:');
        }
        if (trim($this->material) == "") {
            $this->set("material", 'made from:');
        }
        if (trim($this->roof) == "") {
            $this->set("roof", 'with a roof shape that&#039;s:');
        }
        if (trim($this->exactmatch) == "") {
            $this->set("exactmatch", 'I know the exact product name');
        }
        if (trim($this->searchbutton) == "") {
            $this->set("searchbutton", 'search');
        }
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
    }

    protected function addEdit()
    {
        $this->set('btFieldsRequired', $this->btFieldsRequired);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("category", $this->btFieldsRequired) && (trim($args["category"]) == "")) {
            $e->add(t("The %s field is required.", t("Category")));
        }
        if (in_array("material", $this->btFieldsRequired) && (trim($args["material"]) == "")) {
            $e->add(t("The %s field is required.", t("Material")));
        }
        if (in_array("roof", $this->btFieldsRequired) && (trim($args["roof"]) == "")) {
            $e->add(t("The %s field is required.", t("Roof")));
        }
        if (in_array("exactmatch", $this->btFieldsRequired) && (trim($args["exactmatch"]) == "")) {
            $e->add(t("The %s field is required.", t("Exact match")));
        }
        if (in_array("searchbutton", $this->btFieldsRequired) && (trim($args["searchbutton"]) == "")) {
            $e->add(t("The %s field is required.", t("Search Button")));
        }
        return $e;
    }

    public function composer()
    {
        $this->edit();
    }
}
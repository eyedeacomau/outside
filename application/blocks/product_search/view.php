<?php defined("C5_EXECUTE") or die("Access Denied.");
$categories = $controller->getTopics('category');
$materials = $controller->getTopics('material');
$roofs = $controller->getTopics('roof shape');
?>

<form action="/search" method="GET" id="search" class="form-inline">
	<div class="container">
		<div class="row">
			<div class="col-sm-5">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-5">
							<label class="control-label" for="category">
								<?php if(isset($category) && trim($category) != ""): ?><?=h($category); ?><?php endif; ?>
							</label>
						</div>
						<div class="col-sm-7">
							<select class="select2 form-control" id="category" name="category" data-placeholder="All Categories" style="width: 100%;">
								<option></option>
								<?php foreach($categories as $category_option): ?>
									<option value="<?=$category_option['id'];?>"><?=$category_option['name'];?></option>
								<?php endforeach; ?>
								<?php if(isset($exactmatch) && trim($exactmatch) != ""): ?><option value="exact"><?=h($exactmatch); ?></option><?php endif; ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-7">
				<div class="row">
					<div class="col-sm-9">
						<div id="material-group" class="form-group">
							<div class="row">
								<div class="col-sm-4">
									<label class="control-label" for="material">
										<?php if(isset($material) && trim($material) != ""): ?><?=h($material); ?><?php endif; ?>
									</label>
								</div>
								<div class="col-sm-8">
									<select class="select2 form-control" id="material" name="material" data-placeholder="Any Material" style="width: 100%;">
										<option></option>
										<?php foreach($materials as $material_option): ?>
											<option value="<?=$material_option['id'];?>"><?=$material_option['name'];?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
						<div id="roof-group" class="form-group hidden-group">
							<div class="row">
								<div class="col-sm-4">
									<label class="control-label" for="roof">
										<?php if(isset($roof) && trim($roof) != ""): ?><?=h($roof); ?><?php endif; ?>
									</label>
								</div>
								<div class="col-sm-8">
									<select class="select2 form-control" id="roof" name="roof" data-placeholder="Any Roof" style="width: 100%;">
										<option></option>
										<?php foreach($roofs as $roof_option): ?>
											<option value="<?=$roof_option['id'];?>"><?=$roof_option['name'];?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
						<div id="exact-group" class="form-group hidden-group">
							<div class="row">
								<div class="col-sm-4">
									<label class="control-label" for="exact">this product:</label>
								</div>
								<div class="col-sm-8">
									<select class="select2 form-control" id="exact" name="exact" data-placeholder="Search" style="width: 100%;">
										<option></option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<button type="submit" class="btn btn-primary"><?php if(isset($searchbutton) && trim($searchbutton) != ""): ?><?=h($searchbutton); ?><?php endif; ?> <i class="fa fa-search"></i></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

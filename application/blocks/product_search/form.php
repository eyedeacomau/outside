<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<div class="form-group">
    <?php  echo $form->label('category', t("Category")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('category', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('category'), $category, array (
  'maxlength' => 255,
  'placeholder' => 'I&#039;m looking for:',
)); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('material', t("Material")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('material', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('material'), $material, array (
  'maxlength' => 255,
  'placeholder' => 'made from:',
)); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('roof', t("Roof")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('roof', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('roof'), $roof, array (
  'maxlength' => 255,
  'placeholder' => 'with a roof shape that&#039;s:',
)); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('exactmatch', t("Exact match")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('exactmatch', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('exactmatch'), $exactmatch, array (
  'maxlength' => 255,
  'placeholder' => 'I know the exact product name',
)); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('searchbutton', t("Search Button")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('searchbutton', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('searchbutton'), $searchbutton, array (
  'maxlength' => 255,
  'placeholder' => 'search',
)); ?>
</div>
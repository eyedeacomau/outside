<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<div class="form-group">
    <?php  echo $form->label('heading', t("Heading")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('heading', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('heading'), $heading, array (
  'maxlength' => 255,
  'placeholder' => 'Related products',
)); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('productone', t("Product 1")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('productone', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/form/page_selector")->selectPage($view->field('productone'), $productone); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('producttwo', t("Product 2")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('producttwo', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/form/page_selector")->selectPage($view->field('producttwo'), $producttwo); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('productthree', t("Product 3")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('productthree', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/form/page_selector")->selectPage($view->field('productthree'), $productthree); ?>
</div>
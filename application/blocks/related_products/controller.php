<?php  namespace Application\Block\RelatedProducts;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Page;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array();
    protected $btExportFileColumns = array();
    protected $btTable = 'btRelatedProducts';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = true;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Related Products");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->heading;
        return implode(" ", $content);
    }

    public function view()
    {
        if (trim($this->heading) == "") {
            $this->set("heading", 'Related products');
        }
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
    }

    protected function addEdit()
    {
        $this->set('btFieldsRequired', $this->btFieldsRequired);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("heading", $this->btFieldsRequired) && (trim($args["heading"]) == "")) {
            $e->add(t("The %s field is required.", t("Heading")));
        }
        if (in_array("productone", $this->btFieldsRequired) && (trim($args["productone"]) == "" || $args["productone"] == "0" || (($page = Page::getByID($args["productone"])) && $page->error !== false))) {
            $e->add(t("The %s field is required.", t("Product 1")));
        }
        if (in_array("producttwo", $this->btFieldsRequired) && (trim($args["producttwo"]) == "" || $args["producttwo"] == "0" || (($page = Page::getByID($args["producttwo"])) && $page->error !== false))) {
            $e->add(t("The %s field is required.", t("Product 2")));
        }
        if (in_array("productthree", $this->btFieldsRequired) && (trim($args["productthree"]) == "" || $args["productthree"] == "0" || (($page = Page::getByID($args["productthree"])) && $page->error !== false))) {
            $e->add(t("The %s field is required.", t("Product 3")));
        }
        return $e;
    }

    public function composer()
    {
        $this->edit();
    }
}
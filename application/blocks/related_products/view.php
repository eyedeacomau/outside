<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="related">
	<div class="container">
		<?php if(isset($heading) && trim($heading) != ""): ?>
			<div class="row">
				<div class="col-xs-12">
					<h3><?=h($heading); ?></h3>
				</div>
			</div>
		<?php endif; ?>
		<div class="row">
			<?php if(!empty($productone) && ($productone_c = Page::getByID($productone)) && !$productone_c->error && !$productone_c->isInTrash()): ?>
				<?php
					$title = $productone_c->getCollectionName();
					$url = $productone_c->getCollectionLink();
					$custom = (intval($productone_c->getAttribute('custom')) == 1 ? true : false); // Checkboxes can return 1 (ticked) 0 (unticked) false (not set)
					$product_image = $productone_c->getAttribute('product_image');
					if($product_image) $product_image = $product_image->getURL();
				?>
				<div class="col-xs-12 col-sm-4 product<?=($custom ? ' custom' : '');?>">
					<a href="<?=$url;?>" class="thumb" style="background-image: url(<?=$product_image; ?>);">
						<h2><?=$title;?></h2>
					</a>
				</div>
			<?php endif; ?>
			<?php if(!empty($producttwo) && ($producttwo_c = Page::getByID($producttwo)) && !$producttwo_c->error && !$producttwo_c->isInTrash()): ?>
				<?php
					$title = $producttwo_c->getCollectionName();
					$url = $producttwo_c->getCollectionLink();
					$custom = (intval($producttwo_c->getAttribute('custom')) == 1 ? true : false); // Checkboxes can return 1 (ticked) 0 (unticked) false (not set)
					$product_image = $producttwo_c->getAttribute('product_image');
					if($product_image) $product_image = $product_image->getURL();
				?>
				<div class="col-xs-12 col-sm-4 product<?=($custom ? ' custom' : '');?>">
					<a href="<?=$url;?>" class="thumb" style="background-image: url(<?=$product_image; ?>);">
						<h2><?=$title;?></h2>
					</a>
				</div>
			<?php endif; ?>
			<?php if(!empty($productthree) && ($productthree_c = Page::getByID($productthree)) && !$productthree_c->error && !$productthree_c->isInTrash()): ?>
				<?php
					$title = $productthree_c->getCollectionName();
					$url = $productthree_c->getCollectionLink();
					$custom = (intval($productthree_c->getAttribute('custom')) == 1 ? true : false); // Checkboxes can return 1 (ticked) 0 (unticked) false (not set)
					$product_image = $productthree_c->getAttribute('product_image');
					if($product_image) $product_image = $product_image->getURL();
				?>
				<div class="col-xs-12 col-sm-4 product<?=($custom ? ' custom' : '');?>">
					<a href="<?=$url;?>" class="thumb" style="background-image: url(<?=$product_image; ?>);">
						<h2><?=$title;?></h2>
					</a>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

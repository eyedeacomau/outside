<?php
namespace Application\Block\Form;
use Concrete\Core\Block\BlockController;
use Core;
use Database;
use User;
use Page;
use UserInfo;
use Exception;
use FileImporter;
use FileSet;
use File;
use Config;
use Concrete\Core\File\Version;
use Concrete\Block\Form\Controller as FormBlockController;

class Controller extends FormBlockController {

	/**
     * Users submits the completed survey.
     *
     * @param int $bID
     */
    public function action_submit_form($bID = false)
    {
        if ($this->bID != $bID) {
            return false;
        }

        $ip = Core::make('helper/validation/ip');
        $this->view();

        if ($ip->isBanned()) {
            $this->set('invalidIP', $ip->getErrorMessage());

            return;
        }

        $txt = Core::make('helper/text');
        $db = Database::connection();

        //question set id
        $qsID = intval($_POST['qsID']);
        if ($qsID == 0) {
            throw new Exception(t("Oops, something is wrong with the form you posted (it doesn't have a question set id)."));
        }

        //get all questions for this question set
        $rows = $db->GetArray("SELECT * FROM {$this->btQuestionsTablename} WHERE questionSetId=? AND bID=? order by position asc, msqID", array($qsID, intval($this->bID)));

        $errorDetails = array();

        // check captcha if activated
        if ($this->displayCaptcha) {
            $captcha = Core::make('helper/validation/captcha');
            if (!$captcha->check()) {
                $errors['captcha'] = t("Incorrect captcha code");
                $_REQUEST['ccmCaptchaCode'] = '';
            }
        }

        //checked required fields
        foreach ($rows as $row) {
            if ($row['inputType'] == 'datetime') {
                if (!isset($datetime)) {
                    $datetime = Core::make('helper/form/date_time');
                }
                $translated = $datetime->translate('Question'.$row['msqID']);
                if ($translated) {
                    $_POST['Question'.$row['msqID']] = $translated;
                }
            }
            if (intval($row['required']) == 1) {
                $notCompleted = 0;
                if ($row['inputType'] == 'email') {
                    if (!Core::make('helper/validation/strings')->email($_POST['Question' . $row['msqID']])) {
                        $errors['emails'] = t('You must enter a valid email address.');
                        $errorDetails[$row['msqID']]['emails'] = $errors['emails'];
                    }
                }
                if ($row['inputType'] == 'checkboxlist') {
                    $answerFound = 0;
                    foreach ($_POST as $key => $val) {
                        if (strstr($key, 'Question'.$row['msqID'].'_') && strlen($val)) {
                            $answerFound = 1;
                        }
                    }
                    if (!$answerFound) {
                        $notCompleted = 1;
                    }
                } elseif ($row['inputType'] == 'fileupload') {
                    if (!isset($_FILES['Question'.$row['msqID']]) || !is_uploaded_file($_FILES['Question'.$row['msqID']]['tmp_name'])) {
                        $notCompleted = 1;
                    }
                } elseif (!strlen(trim($_POST['Question'.$row['msqID']]))) {
                    $notCompleted = 1;
                }
                if ($notCompleted) {
                    $errors['CompleteRequired'] = t("Complete required fields *");
                    $errorDetails[$row['msqID']]['CompleteRequired'] = $errors['CompleteRequired'];
                }
            }
        }

        //try importing the file if everything else went ok
        $tmpFileIds = array();
        if (!count($errors)) {
            foreach ($rows as $row) {
                if ($row['inputType'] != 'fileupload') {
                    continue;
                }
                $questionName = 'Question'.$row['msqID'];
                if (!intval($row['required']) &&
                    (
                    !isset($_FILES[$questionName]['tmp_name']) || !is_uploaded_file($_FILES[$questionName]['tmp_name'])
                    )
                ) {
                    continue;
                }
                $fi = new FileImporter();
                $resp = $fi->import($_FILES[$questionName]['tmp_name'], $_FILES[$questionName]['name']);
                if (!($resp instanceof Version)) {
                    switch ($resp) {
                    case FileImporter::E_FILE_INVALID_EXTENSION:
                        $errors['fileupload'] = t('Invalid file extension.');
                        $errorDetails[$row['msqID']]['fileupload'] = $errors['fileupload'];
                        break;
                    case FileImporter::E_FILE_INVALID:
                        $errors['fileupload'] = t('Invalid file.');
                        $errorDetails[$row['msqID']]['fileupload'] = $errors['fileupload'];
                        break;

                }
                } else {
                    $tmpFileIds[intval($row['msqID'])] = $resp->getFileID();
                    if (intval($this->addFilesToSet)) {
                        $fs = new FileSet();
                        $fs = $fs->getByID($this->addFilesToSet);
                        if ($fs->getFileSetID()) {
                            $fs->addFileToSet($resp);
                        }
                    }
                }
            }
        }

        if (count($errors)) {
            $this->set('formResponse', t('Please correct the following errors:'));
            $this->set('errors', $errors);
            $this->set('errorDetails', $errorDetails);
        } else { //no form errors
            //save main survey record
            $u = new User();
            $uID = 0;
            if ($u->isRegistered()) {
                $uID = $u->getUserID();
            }
            $q = "insert into {$this->btAnswerSetTablename} (questionSetId, uID) values (?,?)";
            $db->query($q, array($qsID, $uID));
            $answerSetID = $db->Insert_ID();
            $this->lastAnswerSetId = $answerSetID;

            $questionAnswerPairs = array();

            if (Config::get('concrete.email.form_block.address') && strstr(Config::get('concrete.email.form_block.address'), '@')) {
                $formFormEmailAddress = Config::get('concrete.email.form_block.address');
            } else {
                $adminUserInfo = UserInfo::getByID(USER_SUPER_ID);
                $formFormEmailAddress = $adminUserInfo->getUserEmail();
            }
            $replyToEmailAddress = $formFormEmailAddress;
            //loop through each question and get the answers
            foreach ($rows as $row) {
                //save each answer
                $answerDisplay = '';
                if ($row['inputType'] == 'checkboxlist') {
                    $answer = array();
                    $answerLong = "";
                    $keys = array_keys($_POST);
                    foreach ($keys as $key) {
                        if (strpos($key, 'Question'.$row['msqID'].'_') === 0) {
                            $answer[] = $txt->sanitize($_POST[$key]);
                        }
                    }
                } elseif ($row['inputType'] == 'text') {
                    $answerLong = $txt->sanitize($_POST['Question'.$row['msqID']]);
                    $answer = '';
                } elseif ($row['inputType'] == 'fileupload') {
                    $answerLong = "";
                    $answer = intval($tmpFileIds[intval($row['msqID'])]);
                    if ($answer > 0) {
                        $answerDisplay = File::getByID($answer)->getVersion()->getDownloadURL();
                    } else {
                        $answerDisplay = t('No file specified');
                    }
                } elseif ($row['inputType'] == 'url') {
                    $answerLong = "";
                    $answer = $txt->sanitize($_POST['Question'.$row['msqID']]);
                } elseif ($row['inputType'] == 'email') {
                    $answerLong = "";
                    $answer = $txt->sanitize($_POST['Question'.$row['msqID']]);
                    if (!empty($row['options'])) {
                        $settings = unserialize($row['options']);
                        if (is_array($settings) && array_key_exists('send_notification_from', $settings) && $settings['send_notification_from'] == 1) {
                            $email = $txt->email($answer);
                            if (!empty($email)) {
                                $replyToEmailAddress = $email;
                            }
                        }
                    }
                } elseif ($row['inputType'] == 'telephone') {
                    $answerLong = "";
                    $answer = $txt->sanitize($_POST['Question'.$row['msqID']]);
                } else {
                    $answerLong = "";
                    $answer = $txt->sanitize($_POST['Question'.$row['msqID']]);
                }

                if (is_array($answer)) {
                    $answer = implode(',', $answer);
                }

                $questionAnswerPairs[$row['msqID']]['question'] = $row['question'];
                $questionAnswerPairs[$row['msqID']]['answer'] = $txt->sanitize($answer.$answerLong);
                $questionAnswerPairs[$row['msqID']]['answerDisplay'] = strlen($answerDisplay) ? $answerDisplay : $questionAnswerPairs[$row['msqID']]['answer'];

                $v = array($row['msqID'],$answerSetID,$answer,$answerLong);
                $q = "insert into {$this->btAnswersTablename} (msqID,asID,answer,answerLong) values (?,?,?,?)";
                $db->query($q, $v);
            }
            $foundSpam = false;

            $postcode = '';
            $submittedData = '';
            foreach ($questionAnswerPairs as $questionAnswerPair) {
                $submittedData .= $questionAnswerPair['question']."\r\n".$questionAnswerPair['answer']."\r\n"."\r\n";
                if(strtolower($questionAnswerPair['question']) == 'postcode') $postcode = $questionAnswerPair['answer'];
            }
            $antispam = Core::make('helper/validation/antispam');
            if (!$antispam->check($submittedData, 'form_block')) {
                // found to be spam. We remove it
                $foundSpam = true;
                $q = "delete from {$this->btAnswerSetTablename} where asID = ?";
                $v = array($this->lastAnswerSetId);
                $db->Execute($q, $v);
                $db->Execute("delete from {$this->btAnswersTablename} where asID = ?", array($this->lastAnswerSetId));
            }

            if (intval($this->notifyMeOnSubmission) > 0 && !$foundSpam) {
                if (Config::get('concrete.email.form_block.address') && strstr(Config::get('concrete.email.form_block.address'), '@')) {
                    $formFormEmailAddress = Config::get('concrete.email.form_block.address');
                } else {
                    $adminUserInfo = UserInfo::getByID(USER_SUPER_ID);
                    $formFormEmailAddress = $adminUserInfo->getUserEmail();
                }


                $mh = Core::make('helper/mail');
                $mh->to($this->recipientEmail);
                $mh->from($formFormEmailAddress);
                $mh->replyto($replyToEmailAddress);
                $mh->addParameter('formName', $this->surveyName);
                $mh->addParameter('questionSetId', $this->questionSetId);
                $mh->addParameter('questionAnswerPairs', $questionAnswerPairs);
                $mh->load('block_form_admin');
                $mh->setSubject(t('Outside Products %s', $this->surveyName));
                //echo $mh->body.'<br>';
                @$mh->sendMail();

                if($replyToEmailAddress != $formFormEmailAddress) {
                    $mh2 = Core::make('helper/mail');
                    $mh2->to($replyToEmailAddress);
                    $mh2->from($formFormEmailAddress);
                    $mh2->replyto($formFormEmailAddress);
                    $mh2->addParameter('formName', $this->surveyName);
                    $mh2->addParameter('questionSetId', $this->questionSetId);
                    $mh2->addParameter('questionAnswerPairs', $questionAnswerPairs);
                    $mh2->load('block_form_submitter');
                    $mh2->setSubject(t('Outside Products %s', $this->surveyName));
                    //echo $mh2->body.'<br>';
                    @$mh2->sendMail();
                }
                if(!empty($postcode)) {
                    $state = $this->findState($postcode);
                    $agent_email = '';
                    switch($state) {
                        default: break;
                        case 'NSW': case 'ACT': $agent_email = 'nsw@outside.net.au'; break;
                        case 'QLD': $agent_email = 'qld@outside.net.au'; break;
                        case 'VIC': $agent_email = 'vic@outside.net.au'; break;
                        case 'WA': case 'NT': case 'SA': $agent_email = 'wa@outside.net.au'; break;
                        case 'TAS': $agent_email = 'tas@outside.net.au'; break;
                    }
                    if(!empty($agent_email)) {
                        $mh3 = Core::make('helper/mail');
                        $mh3->to($agent_email);
                        $mh3->from($formFormEmailAddress);
                        $mh3->replyto($replyToEmailAddress);
                        $mh3->addParameter('formName', $this->surveyName);
                        $mh3->addParameter('questionSetId', $this->questionSetId);
                        $mh3->addParameter('questionAnswerPairs', $questionAnswerPairs);
                        $mh3->load('block_form_admin');
                        $mh3->setSubject(t('Outside Products %s - '.$state, $this->surveyName));
                        //echo $mh3->body.'<br>';
                        @$mh3->sendMail();
                    }
                }
            }

            if (!$this->noSubmitFormRedirect) {
                if ($this->redirectCID > 0) {
                    $pg = Page::getByID($this->redirectCID);
                    if (is_object($pg) && $pg->cID) {
                        $this->redirect($pg->getCollectionPath());
                    }
                }
                $c = Page::getCurrentPage();
                header("Location: ".Core::make('helper/navigation')->getLinkToCollection($c, true)."?surveySuccess=1&qsid=".$this->questionSetId."#formblock".$this->bID);
                exit;
            }
        }
    }
    public function findState($postcode) {
      $ranges = array(
        'NSW' => array(
          1000, 1999,
          2000, 2599,
          2619, 2898,
          2921, 2999
        ),
        'ACT' => array(
          200, 299,
          2600, 2618,
          2900, 2920
        ),
        'VIC' => array(
          3000, 3999,
          8000, 8999
        ),
        'QLD' => array(
          4000, 4999,
          9000, 9999
        ),
        'SA' => array(
          5000, 5999
        ),
        'WA' => array(
          6000, 6797,
          6800, 6999
        ),
        'TAS' => array(
          7000, 7999
        ),
        'NT' => array(
          800, 999
        )
      );
      $exceptions = array(
        872 => 'NT',
        2540 => 'NSW',
        2611 => 'ACT',
        2620 => 'NSW',
        3500 => 'VIC',
        3585 => 'VIC',
        3586 => 'VIC',
        3644 => 'VIC',
        3707 => 'VIC',
        2899 => 'NSW',
        6798 => 'WA',
        6799 => 'WA',
        7151 => 'TAS'
      );

      $postcode = intval($postcode);
      if ( array_key_exists($postcode, $exceptions) ) {
        return $exceptions[$postcode];
      }

      foreach ($ranges as $state => $range) {
        $c = count($range);
        for ($i = 0; $i < $c; $i+=2) {
          $min = $range[$i];
          $max = $range[$i+1];
          if ( $postcode >= $min && $postcode <= $max ) {
            return $state;
          }
        }
      }

      return null;
    }
}